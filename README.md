# Proyecto Integrador <br> BEBOK tienda virtual para el intercambio de libros.
---
[[_TOC_]]

## Problema a resolver
El creciente precio de los libros, hace que se busquen nuevas formas para su adquisición, buscarlo en formato electronico es una opcion, sin embargo para las personas que no estan acostumbradas a la lectura en este formato, la compra de libros de segunda era la opcion mas favorable, sin embargo el mercado negro de libros de segunda mano, no esta regulado; por lo cual, Bebok nace como una plataforma virtual para el intercambio de libros donde no solo vas a poder transar en dinero, sino que tambien puedes definir por cuales libros harias un trueque y si no tienes ideas de que nuevos libros leer puedes dejarte  tentar por la sugerencia de otros lectores que tambien buscan intercambiar el libro que tu tienes.

#### Casos de uso
Para el MVP de inicio de la plataforma se priorizo el CRUD para la gestion de los libros, trabajando los siguientes casos de uso:
##### REQUERIMIENTOS FUNCIONALES.
* Gestión de producto - Listar productos.
* Gestión de producto - Crear producto.
* Gestión de producto - Editar producto.
* Gestión de producto - Eliminar producto.

##### REQUERIMIENTOS NO FUNCIONALES.
* Backend - Servicios REST para gestion de productos

#### Out of Scope (casos de uso No Soportados)
Por estartegia de priorizacion se desestimo trabajar las siguientes epicas:

##### REQUERIMIENTOS FUNCIONALES.
* Gestión de usuarios - Crear usuario.
* Gestión de usuarios - Editar usuario.
* Gestión de usuarios - Eliminar usuario.

##### REQUERIMIENTOS NO FUNCIONALES.
* Backend - Servicios REST para gestion de usuarios.
---

## ARQUITECTURA

### Diagramas.
poner diagramas de secuencia, uml, etc

#### Diagrama de secuencia.

![alt text](src/assets/Diagramas/Secuencia.png "Diagrama de secuencia")

#### Diagrama de despliegue.

![alt text](src/assets/Diagramas/Despliegue.png "Diagrama de despliegue")

### Diagrama de casos de uso.
![alt text](src/assets/Diagramas/CasosUso.png "Diagrama de M-E-R")

### Modelo de datos
Poner diseño de entidades, Jsons, tablas, diagramas entidad relación, etc..
![alt text](src/assets/Diagramas/MER.png "Diagrama de M-E-R")

## Limitaciones
Lista de limitaciones conocidas.
* Por limitacion de seguridad solo se puede abrir la URL del front en HTTP
* Las funcionalidades de busqueda y propuesta de intermambio de libro no estan disponibles.

## URL de consumo.
* Para el frontend, la aplicación se desplego en la siguiente URL: https://pruebatienda-6147b.web.app/
* Para el backend, la aplicación  se desplego en la siguiiente URL: https://44.210.117.219/tienda-virtual/api/products/
* Para el servidor de archivos, la aplicación se desplego en la siguiente URL: https://7u13zc.deta.dev/

## Mapa de endpoints
Tabla con el listad de endpoints que se usan desde la tienda virtual
| Endpoint | Funcion | Parámetro | Descripción |
| ---      | ---      | ---      | ---      |
| /products/all   | getAll()  | N/A  | Mostrar información de todos los libros.  |
| /products/{id}   | getProduct(id)  | {id}  | Mostrar información de todos los libros.  |
| /products/save   | save(id)  | {id}  | Guardar información de un nuevo producto.  |
| /products/update/{id}   | update()  | {id}  | Actualizar información de un libro específico.  |
| /products/delete/{id}   | delete(id)  | {id}  | Eliminar un libro específico.  |
| /upload   | upload(file)  | {id}  | Cargar imagen de un libro.  |

## Detalles tecnicos.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.3.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
